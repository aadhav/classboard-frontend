//@ts-nocheck
import { Routes, Route } from "react-router-dom";
import SignIn from "./Pages/Singin";
import SignUp from "./Pages/Signup";
import Home from "./Pages/Home";
function App() {
  return (
    <Routes>
      <Route path="/" element={<SignIn />}></Route>
      <Route path="/signup" element={<SignUp />}></Route>
      <Route path="/home" element={<Home />}></Route>
    </Routes>
  );
}

export default App;
