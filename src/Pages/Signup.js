//@ts-nocheck

import React, { useState } from "react";
import Axios from "axios";
import { createSearchParams, Link, useNavigate } from "react-router-dom";

const SignUp = () => {
  const navigate = useNavigate();
  const url = "http://localhost:5000";
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [fullName, setFullName] = useState("");
  const [universityId, setUniversityId] = useState("");
  const [isTeacher, setIsTeacher] = useState("");

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  };
  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  };
  const handleFullNameChange = (e) => {
    setFullName(e.target.value);
  };
  const handleUniversityIdChange = (e) => {
    setUniversityId(e.target.value);
  };


  const ChangePage = (fullName, UniversityId, isTeacher) => {
    navigate({
      pathname: "/home",
      search: createSearchParams({
        name: fullName,
        UniId: UniversityId,
        isTeacher: isTeacher,
      }).toString(),
    });
  };
  const handleSubmit = (e) => {
    // ${url}
    Axios.post(`${url}/users/signup`, { email: email, password: password, fullName: fullName, universityId: universityId, isTeacher: false }).then(
      (res) => {
        if (res.data.error) {
          console.error(res.data.error);
        } else {
          const { fullName, UniversityId, isTeacher } = res.data;
          ChangePage(fullName, UniversityId, isTeacher);
        }
      }
    );
    // ChangePage("Aman", 24, true);
  };

  return (
    <div className="App">
      <label htmlFor="email">Email</label>
      <br />
      <br />
      <input
        name="email"
        type="text"
        value={email}
        placeholder="Email"
        onChange={handleEmailChange}
      />
      <br />
      <br />
      <label htmlFor="password">Password</label>
      <br />
      <br />
      <input
        name="passowrd"
        type="password"
        value={password}
        placeholder="Password"
        onChange={handlePasswordChange}
      />
      <br />
      <label htmlFor="fullname">Full name</label>
      <br />
      <br />
      <input
        name="fullname"
        type="text"
        value={fullName}
        placeholder="Full name"
        onChange={handleFullNameChange}
      />
      <br />
      <label htmlFor="universityId">University ID</label>
      <br />
      <br />
      <input
        name="universityId"
        type="text"
        value={universityId}
        placeholder="University ID"
        onChange={handleUniversityIdChange}
      />
      <br />
      <br />
      <button onClick={handleSubmit}>Sign Up</button>
    </div>
  );
};

export default SignUp;
