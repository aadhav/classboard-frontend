import React from "react";
import { useSearchParams } from "react-router-dom";
const Home = () => {
  const [searchParams] = useSearchParams();
  console.log(searchParams);
  return (
    <>
      {`The name is ${searchParams.get(
        "name"
      )}, the university id is ${searchParams.get("UniId")} and the 
                student is `}
      {console.log(searchParams.get("isTeacher"))}
      {searchParams.get("isTeacher") === "true" ? "a teacher" : "not a teacher"}
    </>
  );
};

export default Home;
