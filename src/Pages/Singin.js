//@ts-nocheck

import React, { useState } from "react";
import Axios from "axios";
import { createSearchParams, Link, useNavigate } from "react-router-dom";

const SignIn = () => {
  const navigate = useNavigate();
  const url = "http://localhost:5000";
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  };
  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  };
  const ChangePage = (fullName, UniversityId, isTeacher) => {
    navigate({
      pathname: "/home",
      search: createSearchParams({
        name: fullName,
        UniId: UniversityId,
        isTeacher: isTeacher,
      }).toString(),
    });
  };
  const handleSubmit = (e) => {
    // ${url}
    Axios.post(`${url}/users/signin`, { email: email, password: password }).then(
      (res) => {
        if (res.data.error) {
          console.error(res.data.error);
        } else {
          const { fullName, UniversityId, isTeacher } = res.data;
          ChangePage(fullName, UniversityId, isTeacher);
        }
      }
    );
    // ChangePage("Aman", 24, true);
  };

  return (
    <div className="App">
      <label htmlFor="email">Email</label>
      <br />
      <br />
      <input
        name="email"
        type="text"
        value={email}
        placeholder="Email"
        onChange={handleEmailChange}
      />
      <br />
      <br />
      <label htmlFor="password">Password</label>
      <br />
      <br />
      <input
        name="passowrd"
        type="password"
        value={password}
        placeholder="Password"
        onChange={handlePasswordChange}
      />
      <br />
      <br />
      <button onClick={handleSubmit}>Sign In</button>
    </div>
  );
};

export default SignIn;
